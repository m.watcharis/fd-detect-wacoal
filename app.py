from flask import Flask
from flask_socketio import SocketIO, send, emit


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'

socketio = SocketIO(app, cors_allowed_origins= "*", engineio_logger=True)

@app.after_request
def after_request(response):
    response.headers.add(
    'Access-Control-Allow-Origin',
    '*',
    )
    response.headers.add(
    'Access-Control-Allow-Credentials',
    'true'
    )
    response.headers.add(
    'Access-Control-Allow-Headers',
    'X-Requested-With,Content-type,withCredentials,authorization'
    )
    response.headers.add(
    'Access-Control-Allow-Methods',
    'GET,PUT,POST,DELETE,OPTIONS'
    )
    return response


@socketio.on('connect')
def serverConnectFrontend():
    print('Client disconnected')
    emit('connect', {'data': 'Client connect.'})


@socketio.on('test')
def test_message_socketio(message):
    print('data >>>', message)
    emit('test', {'message': 'test message'})


@socketio.on('disconnect')
def clientDisconnect():
    print('Client disconnected')
    emit('disconnect',{'data': 'Client disconnect.'})
