import uuid

def generatEUuidShotVersion():
    uuid_test = str(uuid.uuid4())
    new_uuid = uuid_test.split("-")
    return new_uuid[0]

def generatEUuid():
    return str(uuid.uuid4())


def processQueryAllToJson(query):
    result_all = []
    result_data_dic = {}
    for value in query:
        for key, data in value.__dict__.items():
            if key[:1] != '_':
                result_data_dic.update({key:data})

        result_all.append(result_data_dic.copy())
    return result_all

def processQueryJoinToJson(query_sql):
    list_query_sql = []
    for item in query_sql:
        data = {}
        for each_item in item:
            # print("each_item :", each_item.__dict__.items())
            for key,value in each_item.__dict__.items():
                # print("{} : {}".format(key,value))
                if key[:1] != '_':
                    data.update({key:value})
                    # print('data ----------> :', data)
        list_query_sql.append(data)
    # print("list_data ----------> :",list_query_sql)
    return list_query_sql