class PersonDashBoard:
    def __init__(self, time_start, time_end, detect_log_person, db):
        self.time_start = time_start
        self.time_end = time_end
        self.detect_log = detect_log_person
        self.db = db
        self.result_person_dashboard = {
                "person" : "",
                "men" : { 
                    "person" : "", 
                    "percen" : "" 
                    },
                "women" : { 
                    "person" : "", 
                    "percen" : "" 
                    },
                "age" : {
                            "range_1" : {
                                    "person" : "",
                                    "percen" : "",
                                    "men" : {
                                                "person": "",
                                                "percen": ""
                                            },
                                    "women" : {
                                                "person": "",
                                                "percen": ""
                                            }
                                },
                            "range_2" : {
                                    "person" : "",
                                    "percen" : "",
                                    "men" : {
                                                "person": "",
                                                "percen": ""
                                            },  
                                    "women" : {
                                                "person": "",
                                                "percen": ""
                                            }
                                },
                            "range_3" : {
                                    "person" : "",
                                    "percen" : "",
                                    "men" : {
                                                "person": "",
                                                "percen": ""
                                            },
                                    "women" : {
                                                "person": "",
                                                "percen": ""
                                            }
                                },
                            "range_4" : {
                                    "person" : "",
                                    "percen" : "",
                                    "men" : {
                                                "person": "",
                                                "percen": ""
                                            },
                                    "women" : {
                                                "person": "",
                                                "percen": ""
                                            }
                                },
                            "range_5" : {
                                    "person" : "",
                                    "percen" : "",
                                    "men" : {
                                                "person": "",
                                                "percen": ""
                                            },
                                    "women" : {
                                                "person": "",
                                                "percen": ""
                                            }
                                    }
                        }
                    }

    def intialSolutionDashBoard(self):

        check_zone_all = self.db.session.query(self.detect_log).filter(self.detect_log.time.between(self.time_start, self.time_end)).count()
        self.result_person_dashboard['person'] = check_zone_all
        
        check_sex_men = self.db.session.query(self.detect_log)\
            .filter(self.detect_log.sex == "Male", self.detect_log.time.between(self.time_start, self.time_end)).count()

        percen_men = float((check_sex_men/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['men']["person"] = check_sex_men
        self.result_person_dashboard['men']["percen"] = "{:.2f}".format(percen_men)

        check_sex_women = self.db.session.query(self.detect_log)\
            .filter(self.detect_log.sex == "Female", self.detect_log.time.between(self.time_start, self.time_end)).count()
        percen_women = float((check_sex_women/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['women']["person"] = check_sex_women
        self.result_person_dashboard['women']["percen"] = "{:.2f}".format(percen_women)

    ########### age range 20
        person_age_range_twenty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('0', '20'), self.detect_log.time.between(self.time_start, self.time_end)).count()
        count_person_twenty = float((person_age_range_twenty/check_zone_all) * 100) if check_zone_all != 0 else 0
        self.result_person_dashboard['age']["range_1"]["person"] = person_age_range_twenty
        self.result_person_dashboard['age']["range_1"]["percen"] = "{:.2f}".format(count_person_twenty)


        person_men_age_range_twenty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('0', '20'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Male").count()
        count_person_men_twenty = float((person_men_age_range_twenty/check_zone_all) * 100) if check_zone_all != 0 else 0
        self.result_person_dashboard['age']["range_1"]["men"]["person"] = person_men_age_range_twenty
        self.result_person_dashboard['age']["range_1"]["men"]["percen"] = "{:.2f}".format(count_person_men_twenty)

        person_women_age_range_twenty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('0', '20'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Female").count()

        count_person_women_twenty = float((person_women_age_range_twenty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_1"]["women"]["person"] = person_women_age_range_twenty
        self.result_person_dashboard['age']["range_1"]["women"]["percen"] = "{:.2f}".format(count_person_women_twenty)

    ########### age range 40
        person_age_range_forty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('21', '40'), self.detect_log.time.between(self.time_start, self.time_end)).count()
        count_person_forty = float((person_age_range_forty/check_zone_all) * 100) if check_zone_all != 0 else 0
        self.result_person_dashboard['age']["range_2"]["person"] = person_age_range_forty
        self.result_person_dashboard['age']["range_2"]["percen"] = "{:.2f}".format(count_person_forty)

        person_men_age_range_forty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('21', '40'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Male").count()

        count_person_men_forty = float((person_men_age_range_forty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_2"]["men"]["person"] = person_men_age_range_forty
        self.result_person_dashboard['age']["range_2"]["men"]["percen"] = "{:.2f}".format(count_person_men_forty)

        person_women_age_range_forty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('21', '40'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Female").count()

        count_person_women_forty = float((person_women_age_range_forty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_2"]["women"]["person"] = person_women_age_range_forty
        self.result_person_dashboard['age']["range_2"]["women"]["percen"] = "{:.2f}".format(count_person_women_forty)

    ########### age range 60
        person_age_range_sixty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('41', '60'), self.detect_log.time.between(self.time_start, self.time_end)).count()
        count_person_sixty = float((person_age_range_sixty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_3"]["person"] = person_age_range_sixty
        self.result_person_dashboard['age']["range_3"]["percen"] = "{:.2f}".format(count_person_sixty)

        person_men_age_range_sixty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('41', '60'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Male").count()

        count_person_men_sixty = float((person_men_age_range_sixty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_3"]["men"]["person"] = person_men_age_range_sixty
        self.result_person_dashboard['age']["range_3"]["men"]["percen"] = "{:.2f}".format(count_person_men_sixty)

        person_women_age_range_sixty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('41', '60'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Female").count()
        
        count_person_women_sixty = float((person_women_age_range_sixty/check_zone_all) * 100) if check_zone_all != 0 else 0
      
        self.result_person_dashboard['age']["range_3"]["women"]["person"] = person_women_age_range_sixty
        self.result_person_dashboard['age']["range_3"]["women"]["percen"] = "{:.2f}".format(count_person_women_sixty)


    ########### age range 80
        person_age_range_eighty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('61', '80'), self.detect_log.time.between(self.time_start, self.time_end)).count()
        count_person_eighty = float((person_age_range_eighty/check_zone_all) * 100) if check_zone_all != 0 else 0
        
        self.result_person_dashboard['age']["range_4"]["person"] = person_age_range_eighty
        self.result_person_dashboard['age']["range_4"]["percen"] = "{:.2f}".format(count_person_eighty)

        ######## men
        person_men_age_range_eighty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('61', '80'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Male").count()
        count_person_men_eighty = float((person_men_age_range_eighty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_4"]["men"]["person"] = person_men_age_range_eighty
        self.result_person_dashboard['age']["range_4"]["men"]["percen"] = "{:.2f}".format(count_person_men_eighty)

        ####### women
        person_women_age_range_eighty = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('61', '80'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Female").count()
        count_person_women_eighty = float((person_women_age_range_eighty/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_4"]["women"]["person"] = person_women_age_range_eighty
        self.result_person_dashboard['age']["range_4"]["women"]["percen"] = "{:.2f}".format(count_person_women_eighty)


    ############ age range 100
        person_age_range_hundred = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('81', '100'), self.detect_log.time.between(self.time_start, self.time_end)).count()
        count_person_hundred = int((person_age_range_hundred/check_zone_all) * 100) if check_zone_all != 0 else 0
        self.result_person_dashboard['age']["range_5"]["person"] = person_age_range_hundred
        self.result_person_dashboard['age']["range_5"]["percen"] = "{:.2f}".format(count_person_hundred)

        ######## men
        person_men_age_range_hundred = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('81', '100'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Male").count()
        count_person_men_hundred = float((person_men_age_range_hundred/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_5"]["men"]["person"] = person_men_age_range_hundred
        self.result_person_dashboard['age']["range_5"]["men"]["percen"] = "{:.2f}".format(count_person_men_hundred)

        ####### women
        person_women_age_range_hundred = self.db.session.query(self.detect_log.age)\
            .filter(self.detect_log.age.between('81', '100'), self.detect_log.time.between(self.time_start, self.time_end), self.detect_log.sex == "Female").count()

        count_person_women_hundred = float((person_women_age_range_hundred/check_zone_all) * 100) if check_zone_all != 0 else 0

        self.result_person_dashboard['age']["range_5"]["women"]["person"] = person_women_age_range_hundred
        self.result_person_dashboard['age']["range_5"]["women"]["percen"] = "{:.2f}".format(count_person_women_hundred)
        return self.result_person_dashboard
