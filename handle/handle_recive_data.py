from general_funtion import generatEUuid

class ProcessReciveDataFromBox:
    def __init__(self, data, detect_log_person, session, datetime, base64, absolut_path, socketio):
        self.data = data
        self.detect_log = detect_log_person
        self.session = session
        self.datetime = datetime.now()
        self.base64 = base64
        self.absolut_path = absolut_path
        self.socketio = socketio

    def intialSoution(self):
        try:
            person = {
                "member_id": "",
                "permission" :"",
                "age" : "",
                "sex" : "",
                "img_person" : "",
                "time": ""
            }
            
            for item in self.data["data"]:
                uuid = generatEUuid()
                filename = uuid + self.datetime.strftime("%m-%d-%Y_%H:%M:%s") + ".jpg"

                person["member_id"] = item["Member_id"]
                person["permission"] = item["Permission"]
                person["age"] = item["Age"]
                person["sex"] = item["Gender"]
                person["img_person"] = "http://0.0.0.0:6002/api/v1/uploads/" + filename
                person["time"] = self.datetime.strftime("%m-%d-%Y_%H:%M:%s")

                insert_data = self.detect_log(
                    detect_log_people_id = uuid,
                    member_id = item["Member_id"],
                    permission = item["Permission"],
                    age = item["Age"],
                    sex = item["Gender"],
                    device_id = item["Device"],
                    img_person = filename,
                    time = self.datetime,
                    create_by = "system",
                    create_date = self.datetime,
                    edit_date = "-",
                    edit_by = "-"
                )
                self.session.add(insert_data)
                self.session.commit()
            
                self.socketio.emit('person', { "data" :  person }, broadcast=True)
                self.socketio.emit('detect', { "data" : "Api detect request" }, broadcast=True)

                imgdata = self.base64.b64decode(item["Img_person"])
                with open(self.absolut_path + "/img_person/" + filename, 'wb') as f:
                    f.write(imgdata)
            return "insert data success"
        except Exception as e:
            return str(e)
        