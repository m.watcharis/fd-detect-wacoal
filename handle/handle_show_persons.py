class ShowPersons:
    def __init__(self, detect_log_person, db, pocess_query):
        self.detect_log_person = detect_log_person
        self.db = db
        self.pocess_query = pocess_query
    
    def intialProcessShowPersons(self):
        _person = self.pocess_query(self.db.session.query(self.detect_log_person).order_by(self.detect_log_person.time.desc()).limit(10).all())
        show_person = []
        for _loop in _person:
            _loop["img_person"] = "http://0.0.0.0:6002/uploads/" + _loop["img_person"]
            show_person.append(_loop)
        return show_person



