from flask_sqlalchemy import SQLAlchemy
from config_postgres_db import config_db
from app import app
import os

DB_USER = os.getenv('DB_USER', default=config_db["db_user"])
DB_PWD = os.getenv('DB_PWD', default=config_db["db_pwd"])
DB_HOST = os.getenv('DB_HOST', default=config_db["db_host"])
DB_PORT = os.getenv('DB_PORT', default=config_db["db_port"])
DB_NAME = os.getenv('DB_NAME', default=config_db["db_name"])

# DB_URL = f'postgresql+psycopg2://{config_db["username"]}:{config_db["password"]}@{config_db["db_host"]}:{config_db["db_port"]}/{config_db["db_name"]}'
DB_URL = f'postgresql+psycopg2://{DB_USER}:{DB_PWD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
print("create schema success")


app.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True 

db = SQLAlchemy(app)
session = db.session


class DetectLogPersons(db.Model):
    __tablename__='detect_log_people'
    detect_log_people_id = db.Column(db.String, primary_key=True)
    member_id = db.Column(db.String)
    permission = db.Column(db.String)
    age = db.Column(db.Integer)
    sex = db.Column(db.String)
    device_id = db.Column(db.String)
    img_person = db.Column(db.String)
    time = db.Column(db.DateTime)
    create_by = db.Column(db.String)
    create_date = db.Column(db.String)
    edit_date = db.Column(db.String)
    edit_by = db.Column(db.String)

    def __init__(self, detect_log_people_id, member_id, permission, age, sex, device_id, img_person, time, create_by, create_date, edit_date, edit_by):
        self.detect_log_people_id = detect_log_people_id
        self.member_id = member_id
        self.permission = permission
        self.age = age
        self.sex = sex
        self.device_id = device_id
        self.img_person = img_person
        self.time = time 
        self.create_by = create_by
        self.create_date = create_date
        self.edit_date = edit_date
        self.edit_by = edit_by


def execSchemaDb():
    db.create_all()