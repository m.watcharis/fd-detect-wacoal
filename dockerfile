FROM python:3.6

ADD . /fd-detect

WORKDIR /fd-detect

ADD requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

EXPOSE 8005
CMD ["python", "-u", "run.py"]
