from app import app, socketio
from flask import Flask, jsonify, request, send_from_directory
from flask_socketio import SocketIO, emit, send
from connect_db import execSchemaDb, db, session, DetectLogPersons
from handle.handle_recive_data import ProcessReciveDataFromBox
from handle.handle_dashboard import PersonDashBoard
from handle.handle_show_persons import ShowPersons
from general_funtion import generatEUuidShotVersion, generatEUuid, processQueryAllToJson
from datetime import datetime
import os 
import base64


absolut_path = os.path.abspath(os.getcwd())

UPLOAD_FOLDER = "./img_person"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/api/v1/detect', methods=["POST"])
def reciveDataFromBox():
    try:
        data = request.json
        process_data = ProcessReciveDataFromBox(data, DetectLogPersons, session, datetime, base64, absolut_path, socketio)
        result_data = process_data.intialSoution()
        return jsonify({"message": result_data, "status":"success"}),200
    except Exception as e:
        return jsonify({"message": str(e), "status":"fail"}),400


@app.route('/api/v1/uploads/<filename>', methods=['GET'])
def downloadFile(filename):
    try:
        return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)
    except Exception as e:
        return jsonify({"message": str(e), "status":"fail"}),400


@app.route('/api/v1/dashboard', methods=["GET"])
def dashboardPerson():
    try:
        time_start = request.args.get('time_start')
        time_end = request.args.get('time_end')
        process_dashboard = PersonDashBoard(time_start, time_end, DetectLogPersons, db)
        result_process_dashboard = process_dashboard.intialSolutionDashBoard()
        return jsonify({ "message": "dashboardPerson success", "status":"success", "data": result_process_dashboard }),200
    except Exception as e:
        return jsonify({"message": str(e), "status":"fail"}),400


@app.route('/api/v1/person', methods=["GET"])
def showPersons():
    try:
        show_person = ShowPersons(DetectLogPersons, db, processQueryAllToJson)
        result_show_person = show_person.intialProcessShowPersons()
        return jsonify({ "message": "showPersons success", "status":"success", "data": result_show_person}),200
    except Exception as e:
        return jsonify({"message": str(e), "status":"fail"}),400



if __name__ == "__main__":
    execSchemaDb()
    socketio.run(app, host="0.0.0.0", port=6002)
